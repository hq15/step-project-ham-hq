"use strict";
const tabs = document.querySelector(".menu-list-services");
const tabsItem = document.querySelectorAll(".tabs-services > li");
const tabsActive = document.querySelector(".js-tab-active");
const nameAttr = tabsActive.dataset.name;
const workTabs = document.querySelector(".work-menu");
const workTabsItem = document.querySelectorAll(".img-work > img");
const btnLoadMore = document.querySelector(".load-btn");

// создано два массива с адресами картинок и их категорией, соотвественно
const arrImg = [
  "img/wordpress/wordpress9.jpg",
  "img/landing_page/landing-page5.jpg",
  "img/web_design/web-design2.jpg",
  "img/web_design/web-design5.jpg",
  "img/web_design/web-design6.jpg",
  "img/web_design/web-design7.jpg",
  "img/landing_page/landing-page1.jpg",
  "img/landing_page/landing-page3.jpg",
  "img/landing_page/landing-page4.jpg",
  "img/wordpress/wordpress1.jpg",
  "img/wordpress/wordpress2.jpg",
  "img/wordpress/wordpress3.jpg",
  "img/wordpress/wordpress1.jpg",
  "img/wordpress/wordpress2.jpg",
  "img/wordpress/wordpress3.jpg",
  "img/wordpress/wordpress9.jpg",
  "img/wordpress/wordpress9.jpg",
  "img/web_design/web-design5.jpg",
  "img/web_design/web-design6.jpg",
  "img/web_design/web-design7.jpg",
  "img/landing_page/landing-page1.jpg",
  "img/landing_page/landing-page3.jpg",
  "img/landing_page/landing-page4.jpg",
  "img/wordpress/wordpress1.jpg",
];
const arrCat = [
  "Wordpress",
  "Landing Pages",
  "Web Design",
  "Web Design",
  "Web Design",
  "Web Design",
  "Landing Pages",
  "Landing Pages",
  "Landing Pages",
  "Wordpress",
  "Wordpress",
  "Wordpress",
  "Wordpress",
  "Wordpress",
  "Wordpress",
  "Wordpress",
  "Wordpress",
  "Web Design",
  "Web Design",
  "Web Design",
  "Landing Pages",
  "Landing Pages",
  "Landing Pages",
  "Wordpress",
];

// ховер на закладки меню с подчеркиванием внизу блока
const menuItemHeader = document.querySelector(".menu-list-header");
menuItemHeader.addEventListener("mouseover", (event) => {
  let target = event.target;
  if (target.closest(".menu-text-header")) {
    target.style.color = "#18CFAB";
    target.parentNode.style.borderBottomStyle = "solid";
    target.parentNode.style.paddingBottom = "50px";
    target.style.opacity = "1";
    target.style.cursor = "pointer";
  }
});

menuItemHeader.addEventListener("mouseout", (event) => {
  let target = event.target;
  if (target.closest(".menu-text-header")) {
    target.style.color = "#FFFFFF";
    target.style.opacity = "0.6";
    target.parentNode.style.borderBottomStyle = "";
    target.parentNode.style.paddingBottom = "52px";
  }
});

// фильтр секций "Services" и  "Work"
hidingItems(tabsItem, nameAttr);

tabs.addEventListener("click", (event) => {
  const tabsTitleActive = document.querySelector(".js-tab-active");
  tabsTitleActive.classList.remove("js-tab-active");
  const tabItem = event.target.innerHTML;
  event.target.classList.add("js-tab-active");
  hidingItems(tabsItem, tabItem);
});

workTabs.addEventListener("click", (event) => {
  const workTitleActive = document.querySelector(".js-tab-active-work");
  workTitleActive.classList.remove("js-tab-active-work");
  const tabItemWork = event.target.innerHTML;
  event.target.classList.add("js-tab-active-work");
  hidingItemsImg(workTabsItem, tabItemWork);
});

// иммитация загрузка картинок по 12шт в два приема
let counter = 0;
let item = 0;

btnLoadMore.addEventListener("click", (event) => {
  counter = counter + 1;
  const imgTest = document.querySelectorAll(".img-load");
  imgTest.forEach((element) => {
    if (element.getAttribute("src") === "#" && item < 12) {
      downloadImg(arrImg, arrCat, element);
      item = item + 1;
    }

    if (element.getAttribute("src") === "#" && item < 24 && counter > 1) {
      downloadImg(arrImg, arrCat, element);
      item = item + 1;
    }
    if (item === 24) {
      btnLoadMore.hidden = true;
    }
  });
});

//  функция иммитации загрузки картинок

function downloadImg(imgArr, catArr, elementArr) {
  elementArr.src = imgArr.shift();
  let categoryImg = catArr.shift();
  elementArr.alt = categoryImg;
  elementArr.dataset.category = categoryImg;
  elementArr.parentNode.classList.add("div-active");
  console.log(elementArr.parentNode);
  document
    .querySelector(".div-active .work-hover-block-info")
    .insertAdjacentText("afterbegin", elementArr.dataset.category);
  elementArr.parentNode.classList.add("img-work-active");
  elementArr.parentNode.classList.remove("div-active");
  elementArr.parentNode.classList.remove("img-work-empty");
  elementArr.parentNode.classList.add("img-work");
  elementArr.classList.remove("img-load");
}

// Функции фильтра секции "Service" и "Work"

function hidingItems(arr, item) {
  arr.forEach((element) => {
    element.hidden = true;
    if (item === element.dataset.name) element.hidden = false;
  });
}

function hidingItemsImg(arr, item) {
  arr.forEach((element) => {
    element.parentNode.hidden = true;
    if (item === element.dataset.category || item === "All")
      element.parentNode.hidden = false;
  });
}

// карусель

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides((slideIndex += n));
}

function currentSlide(n) {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  let i;
  const slides = document.getElementsByClassName("js-my-slides");
  const dots = document.getElementsByClassName("js-demo");

  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].hidden = true;
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active-slide", "");
  }

  slides[slideIndex - 1].hidden = false;
  dots[slideIndex - 1].className += " active-slide";
}
